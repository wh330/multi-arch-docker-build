# Multi-arch docker build

Example of a CI config for building multi-arch docker images.

## Getting started

To build and run the docker image:
```console
docker build -t multiarch-example .

docker run -it multiarch-example
```

## CI config

See [.gitlab-ci.yml](https://gitlab.developers.cam.ac.uk/wh330/multi-arch-docker-build/-/blob/main/.gitlab-ci.yml)
for an example of how to build docker images for multiple architectures.
