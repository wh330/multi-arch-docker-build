FROM golang:alpine

# Change to /build directory
WORKDIR /build

# Download module dependencies
COPY go.mod ./
RUN go mod download

# Copy hello.go file to /build
COPY hello.go ./

# Build hello module
RUN go build -o ./dist/hello .

# Run hello module
CMD ["/build/dist/hello"]
