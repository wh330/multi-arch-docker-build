// Prints "Hello, World!" to the terminal
package main

import "fmt"

func main() {
	fmt.Println("Hello, World!")
}
